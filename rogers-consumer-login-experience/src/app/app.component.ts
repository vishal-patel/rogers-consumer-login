import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  rogersConsumer = 'Click here to access Rogers consumer';
  fidoConsumer = 'Click here to access Fido consumer';
}
